package com.springcore;

public class Student {
	private int stdId;
	private String stdName;
	private String stdAdd;
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public String getStdAdd() {
		return stdAdd;
	}
	public void setStdAdd(String stdAdd) {
		this.stdAdd = stdAdd;
	}
	// now using constructors
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Student [stdId=" + stdId + ", stdName=" + stdName + ", stdAdd=" + stdAdd + "]";
	}
	public Student(int stdId, String stdName, String stdAdd) {
		super();
		this.stdId = stdId;
		this.stdName = stdName;
		this.stdAdd = stdAdd;
	}
	public void hello()
	{
		//this is just a greeting
		System.out.println("Hello Cutiee");
	}
	
}
